const dictionary = {
    ru: {
        "page-title": "Малозанская книга падших",
        "page-description": "Аномандер Рейк - кумир бисёненов",
    },
    en: {
        "page-title": "Malazan Book of the Fallen",
        "page-description": "Anomander Rake - idol of bisyonenov",
    }
};

const lang = "ru";
const arrLang = dictionary[lang];

const elements = document.querySelectorAll("[data-translate]"); // находим все теги, у которых есть атрубут data-translate

elements.forEach(elem => {
    const phrase = elem.dataset.translate;
    const text = arrLang[phrase];
    elem.textContent = text;
})
